var numbers = [0]
var operation = "";

var f = false;
var firstRun = true;
var wholeOperation = [];

var mem = 0;

function makeNumber() {
	var n = "";
	
	for(var i = 0; i < numbers.length ; i++) {
		n = n + numbers[i];
	}
	
	return n;
}

function checkForZero() {
	var num = makeNumber();
	var z = (num > -1 && num < 1);
	return  z;
}

function addNumberListener(element ,num) {
	$(element).click(function() {
		if(f === true) {
			f = false;
			wholeOperation = [];
			$("#operand").html(wholeOperation);
			numbers = [];
		}
	
		if(num === 0) {
			if(!checkForZero()) {
				
				numbers.push(num);
				$("#currNumber").html(numbers);
		
				var currentSize = $("#tbox").css("font-size");
				currentSize = currentSize.replace("px","");
				if(currentSize * numbers.length > 380) {
					currentSize = currentSize * .90;
					$("#tbox").css("font-size", currentSize+"px");
				}
			}
		} else { 
			if(numbers[0] === 0)
				numbers = [];
				numbers.push(num);
				$("#currNumber").html(makeNumber());
		
				var currentSize = $("#tbox").css("font-size");
				currentSize = currentSize.replace("px","");
				if(currentSize * numbers.length > 380) {
					currentSize = currentSize * .90;
					$("#tbox").css("font-size", currentSize+"px");
				}	
		}	
	});
}

function addSignListeners() {
	$("#clear").click(function() {
		numbers = [0];
		$("#currNumber").html("0");
		$("#tbox").css("font-size", "58px");
		$("#operand").html("");		
	});
	
	$("#clearall").click(function() {
		numbers = [0];
		operation = "";
		$("#currentOperation").html("");
		$("#currNumber").html("0");
		$("#tbox").css("font-size", "58px")	;	
		$("#operand").html("");
		wholeOperation = [];
		firstRun = true;
	});
	
	$("#plus").click(function() {
		doOperation("+");
	});
	
	$("#minus").click(function() {
		doOperation("-");
	});
	
	$("#times").click(function() {
		doOperation("*");
	});
	
	$("#divide").click(function() {
		doOperation("/");
	});
	
	$("#carat").click(function() {
		doOperation("^");
	});
	
	$("#sqrt").click(function() {
		doSQRT();
	});
	
	$("#negative").click(function() {
		doNegative();
	});
	
	$("#decimal").click(function() {
		doDecimal();
	});
	
	$("#memsave").click(function() {
		mem = makeNumber();
		numbers = [0];
		$("#currNumber").html(numbers);
	});
	
	$("#memget").click(function() {
		numbers = [mem];
		$("#currNumber").html(numbers);
	});
	
	$("#equals").click(function() {
		doSolve();
		f = true;
		$("#operand").html("");
	});
	
	$(".sign").on("mousedown mouseup", function(e){
		$(this).toggleClass( "active", e.type === "mousedown" );
	});
	
	$(".number").on("mousedown mouseup", function(e){
		$(this).toggleClass( "active", e.type === "mousedown" );
	});	
}

function doNegative() {
	if(numbers[0] === "-")
		numbers.shift();
	else
		numbers.unshift("-");
		
	$("#currNumber").html(numbers);
}

function doSQRT() {
	if(numbers[0] === "sqrt")
		numbers.shift();
	else
		numbers.unshift("sqrt");
		
	$("#currNumber").html(numbers);
}

function doDecimal() {
	if(numbers.indexOf(".") === -1) {
		numbers.push(".");
		f = false;
	}
	$("#currNumber").html(numbers);
}

function doSolve() {
	if(wholeOperation.length === 0) {
		if(String(makeNumber()).indexOf("sqrt") != -1) {
			var a = Math.round(Math.sqrt(makeNumber().replace("sqrt","")) * 100) / 100;
			$("#currNumber").html(a);
			numbers = [a];
		}
		return;
	}
	wholeOperation.push(operation);
	$("#currentOperation").html("");
	wholeOperation.push(makeNumber());
		
	while(doSquares() != -1) {} 
	
	while(solveFirstOccurance("*","/") != -1) {}
	
	while(solveFirstOccurance("+","-") != -1) {}
	
	$("#currNumber").html(wholeOperation[0])
	operation="";
	$("#currOperation").html("")
	numbers=wholeOperation;
	wholeOperation=[];
	f = true;
	
}

function findFirstOccurance(one, two, arr) {
	for(var i = 0; i < arr.length; i++) {
		if(arr[i] === one || arr[i] === two) {
			return i;
		}
	}
	
	return -1;
}

function doSquares() {
	for(var i = 0; i < wholeOperation.length; i++) {
		if(String(wholeOperation[i]).indexOf("sqrt") != -1) {
			wholeOperation[i] = Math.round(Math.sqrt(wholeOperation[i].replace("sqrt","")) * 100) / 100;
			return 1;
		}
		
		if(wholeOperation[i]==="^") {
			var num1 = wholeOperation[i - 1];
			var num2 = wholeOperation[i + 1];
			wholeOperation[i - 1] = Math.pow(num1, num2);
			wholeOperation.splice(i, 2)
			return 1;
		}
	}
	return -1;
}

function solveFirstOccurance(op1, op2) {
	var index = findFirstOccurance(op1, op2, wholeOperation);

	if(index === -1) {
		return -1;
	} else {
		var num1 = wholeOperation[index-1];
		var num2 = wholeOperation[index+1];
		var operation = wholeOperation[index];

		if(operation === "+") {
			wholeOperation[index - 1] = parseFloat(num1) + parseFloat(num2);
			wholeOperation.splice(index, 2)
		} else if(operation === "-") {
			wholeOperation[index - 1] = parseFloat(num1) - parseFloat(num2);
			wholeOperation.splice(index, 2)
		} else if(operation === "*") {
			wholeOperation[index - 1] = parseFloat(num1) * parseFloat(num2);
			wholeOperation.splice(index, 2)
		} else if(operation === "/") {
			wholeOperation[index - 1] = parseFloat(num1) / parseFloat(num2);
			wholeOperation.splice(index, 2)
		}		
		
		return 1;
	}
}

function doOperation(newOperation) {
	if(!checkForZero()) {
		if(operation != "") {
			wholeOperation.push(operation);
		}
		f = false;
		wholeOperation.push(makeNumber());
		operation = newOperation;
		numbers = [0];
		$("#currentOperation").html(operation);
		$("#operand").html(wholeOperation);
		$("#currNumber").html("0")
	}
}